# Slidedecks

In this project you will find PDF copies of the slidedecks used in my presentations and sessions. You can use them to recap the session or to look something up for reference.

# Copyright
©2021 David de Hoop. All content in the slidedecks within this repository may only be used for commercial purposes and may only be redistributed with my explicit consent. 
